# README #

## USB keyboard settings for Karabiner Elements ##

### Import settings for Karabiner Elements ###

[Install latest MacUsbKeysRules.json from here](https://htmlpreview.github.io/?https://bitbucket.org/juultone/macusbkeys/raw/master/Index.html)

### View settings file in browser ###

[View the latest MacUsbKeysRules.json](https://bitbucket.org/juultone/macusbkeys/raw/master/MacUsbKeysRules.json)